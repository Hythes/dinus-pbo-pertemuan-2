public class AksiLampu {

    public static void main(String[] args){

        Lampu lampuKamar = new Lampu();

        System.out.println("Apakah lampu kamar menyala? "+ lampuKamar.nyala);
        lampuKamar.matikanLampu();
        System.out.println("Apakah lampu kamar menyala? "+ lampuKamar.nyala);

    }

}

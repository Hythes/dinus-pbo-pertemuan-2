public class AksiSepeda {
    public static void main(String[] args) {

        Sepeda sepedaBalap = new Sepeda(2,"Pixie","Adidas");
        Sepeda sepedaRenang = new Sepeda(3,"XoootZ","Asus");

        int gearSepeda = sepedaBalap.gear;
        System.out.println(gearSepeda);
        sepedaBalap.ngerem();

        System.out.println(sepedaRenang.gear);
        sepedaRenang.ngerem();

    }
}
